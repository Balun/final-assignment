import fa.cvision as cvision
import fa.cvision.neural.models as models
import fa.utils as utils

import numpy as np
import keras.backend as backend
import cv2 as cv

"""
x_train, y_train = utils.load('/home/lumba/Programming/Python/završni/final'
                              '-assignment/datasets/training_data', 'train')
x_test, y_test = utils.load('/home/lumba/Programming/Python/završni/final'
                            '-assignment/datasets/test_data', 'test')
"""

x_train = np.load('train/x_data.npy')
y_train = np.load('train/y_data.npy')
x_test = np.load('test/x_data.npy')
y_test = np.load('test/y_data.npy')

x_train = np.array([cvision.grayscale_to_binary(img) for img in x_train])
x_train = cv.bitwise_not(x_train)
x_test = np.array([cvision.grayscale_to_binary(img) for img in x_test])
x_test = cv.bitwise_not(x_test)

print("Data loaded")

batch_size = 128
num_classes = 37
epoch = 3

img_rows, img_cols = 32, 24

if backend.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)

else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = utils.to_categorical(y_train, num_classes)

y_test = utils.to_categorical(y_test, num_classes)
cv.imshow("bla", x_train[0])
cv.waitKey(0)
cv.imshow("bla", x_train[-1])

print(num_classes, input_shape)

model = models.mnist_cnn(num_classes, input_shape)

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epoch,
          verbose=1,
          validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, verbose=0)

model.save('first_model.h5')

print('Test loss:', score[0])
print('Test accuracy:', score[1])
