import fa.cvision.neural as neural
import fa.cvision as cvision
import fa.cvision.neural.engine as engine
import numpy as np
import cv2 as cv

import os

IMG_ROWS, IMG_COLUMNS = 32, 24

y_train = np.load('train/y_data.npy')
x_train = np.load('train/x_data.npy')

model = neural.load_model('first_model.h5')

for sample in cvision.crop_licence_elements(cvision.crop_licence_plate(
        'example3.jpg')):
    height, width = sample.shape
    sample = cvision.resize(sample, IMG_COLUMNS, IMG_ROWS)
    sample = cvision.invert_colors(sample)
    cvision.show_image(sample)

    result = engine.predict(model, sample)
    print(result)
"""
for image in sorted(os.listdir('validation')):
    sample = cvision.read_image(os.path.join('validation', image))
    sample = cvision.invert_colors(cvision.grayscale_to_binary(sample))

    cvision.show_image(sample)
    result = engine.predict(model, sample)
    print(result)
"""
