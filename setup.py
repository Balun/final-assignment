# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='Final Assignment',
    version='0.1.0',
    description='Bsc. Final Assignment,
    long_description=readme,
    author='Matej Balun',
    author_email='matej.balun@fer.hr',
    url='https://gitlab.com/Balun/final-assignment',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

