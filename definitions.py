import os
#import fa.cvision.neural.models.pre_trained as pre_trained_models

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

DATASETS_DIR = os.path.join(ROOT_DIR, 'datasets')

#FIRST_MODEL_PATH = os.path.join(os.path.dirname(os.path.abspath(
#    pre_trained_models.__file__)), 'first_model.h5')

FIRST_MODEL_PATH = os.path.join(ROOT_DIR, 'fa', 'cvision', 'neural',
                                'models', 'pre_trained', 'first_model.h5')

RUNTIME_DATA_PATH = os.path.join(ROOT_DIR, 'lib', 'openalpr', 'runtime_data')

OPENALPR_CONF_PATH = os.path.join('etc', 'openalpr', 'openalpr.conf')
