#!/usr/bin/python3

import argparse
import sys


def get_description():
    """

    :return:
    """
    desc = "This program represents the final assignment project from Matej " \
           "Balun, bsc. student, Universitiy of Zagreb, Faculty of " \
           "Electrical Engineering and Computing.\n"
    desc += "The thesis title is called \"Classification of Images Using " \
            "Deep Learning on an Embedded Computer\".\n"
    desc += "You can run the program in GUI mode or CMD mode.\n"
    desc += "Please read the instructions using \"./fa_app.py -h\"."

    return desc


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=get_description())
    subparsers = parser.add_subparsers(help='modes', dest='mode')

    # train mode
    train_parser = subparsers.add_parser('train',
                                         help='train the model with provided '
                                              'dataset')
    train_parser.add_argument('dataset-dir', help='Path to the dataset ' \
                                                  'directory')
    train_parser.add_argument('--batch-size', help='size of the batch for '
                                                   'training process, '
                                                   'default 128',
                              type=int)
    train_parser.add_argument('--epochs', help='number of epoch for the '
                                               'training process, default 5',
                              type=int)
    train_parser.add_argument('--save-dir', help='Destination directory to '
                                                 'save the model')
    train_parser.add_argument('--save-name', help='Name of the file for '
                                                  'saving the model')
    train_parser.add_argument('--model-path', help='Path to the pre trained '
                                                   'model')

    # cmd mode
    cmd_parser = subparsers.add_parser('cmd', help='run the application in '
                                                   'console mode')
    cmd_parser.add_argument('--model-path', help='path to the trained model '
                                                 '.h5 file')
    cmd_parser.add_argument('sample-path', help='path to the directory with '
                                                'prediction sample or the '
                                                'single sample path')

    # alpr mode
    alpr_parser = subparsers.add_parser('alpr', help='Recognize the plates '
                                                     'automatically via '
                                                     'openalpr library')
    alpr_parser.add_argument('--country', help='the specified licence plate '
                                               'country template, default is '
                                               '\'eu\'')
    alpr_parser.add_argument('sample-path', help='path to the directory with '
                                                 'prediction sample or the '
                                                 'single sample path')

    # gui mode
    gui_parser = subparsers.add_parser('gui', help='run the application in '
                                                   'gui '
                                                   'mode')
    # TODO arguments

    args = parser.parse_args()
    mode = args.mode

    import fa.commands as commands
    import fa.utils as utils

    if not mode:
        mode = commands.GUI

    args = {key.replace('-', '_'): value for key, value in args.__dict__.items(
    ) if (value and key != 'mode')}

    try:
        commands.get(mode).execute(args)
    except utils.PlateRecognitionError as ex:
        sys.stderr.write(str(ex))
