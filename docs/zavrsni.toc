\select@language {croatian}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Opis problema}{2}{chapter.2}
\contentsline {chapter}{\numberline {3}Kori\IeC {\v s}teni razvojni alati i biblioteke}{3}{chapter.3}
\contentsline {section}{\numberline {3.1}Python 3 (3.5.2)}{3}{section.3.1}
\contentsline {section}{\numberline {3.2}Tensorflow}{4}{section.3.2}
\contentsline {section}{\numberline {3.3}Keras}{4}{section.3.3}
\contentsline {section}{\numberline {3.4}OpenCV}{5}{section.3.4}
\contentsline {section}{\numberline {3.5}NumPy}{5}{section.3.5}
\contentsline {section}{\numberline {3.6}OpenALPR}{6}{section.3.6}
\contentsline {section}{\numberline {3.7}PyQt}{6}{section.3.7}
\contentsline {section}{\numberline {3.8}PyCharm}{7}{section.3.8}
\contentsline {section}{\numberline {3.9}Git}{7}{section.3.9}
\contentsline {section}{\numberline {3.10}Linux Mint}{8}{section.3.10}
\contentsline {section}{\numberline {3.11}Raspberry Pi 3 (Model B+)}{8}{section.3.11}
\contentsline {chapter}{\numberline {4}Duboko u\IeC {\v c}enje i konvolucijske neuronske mre\IeC {\v z}e}{10}{chapter.4}
\contentsline {section}{\numberline {4.1}Uvod}{10}{section.4.1}
\contentsline {section}{\numberline {4.2}Unaprijedne neuronske mre\IeC {\v z}e}{11}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Jednoslojni perceptron}{11}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Vi\IeC {\v s}eslojni perceptron}{11}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Osnovni duboki modeli}{12}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Konvolucijske neuronske mre\IeC {\v z}e}{13}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Struktura konvolucijskih neuronskih mre\IeC {\v z}a}{13}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Zna\IeC {\v c}ajke konvolucijskih neuronskih mre\IeC {\v z}a}{14}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}U\IeC {\v c}enje konvolucijskih neuronskih mre\IeC {\v z}a}{15}{chapter.5}
\contentsline {section}{\numberline {5.1}Gradijentni spust}{15}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Varijante gradijentnog spusta}{16}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Algoritam Backpropagation}{17}{section.5.2}
\contentsline {chapter}{\numberline {6}Arhitektura i dizajn sustava}{18}{chapter.6}
\contentsline {section}{\numberline {6.1}Struktura i organizacija programskog rje\IeC {\v s}enja}{18}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Paket fa.commands}{19}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Paket fa.cvision}{20}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Paket fa.inputs}{21}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Paket fa.ui}{23}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}Ostali direktoriji i paketi}{23}{subsection.6.1.5}
\contentsline {section}{\numberline {6.2}Implementacija kori\IeC {\v s}tenog modela za u\IeC {\v c}enje}{24}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Op\IeC {\'c}enito o implementaciji dubokog u\IeC {\v c}enja u sustavu}{24}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}MNIST}{24}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Kori\IeC {\v s}teni model za duboko u\IeC {\v c}enje}{24}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}U\IeC {\v c}enje kori\IeC {\v s}tenog modela}{26}{subsection.6.2.4}
\contentsline {section}{\numberline {6.3}Skup podataka za u\IeC {\v c}enje}{27}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Inicijalni skup podataka}{27}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Skup podataka za dodatno u\IeC {\v c}enje}{27}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}Grafi\IeC {\v c}ko korisni\IeC {\v c}ko su\IeC {\v c}elje}{28}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Recognize file}{28}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Raspberry Pi}{29}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}Train}{30}{subsection.6.4.3}
\contentsline {section}{\numberline {6.5}Naredbeni redak}{31}{section.6.5}
\contentsline {section}{\numberline {6.6}Raspberry Pi}{32}{section.6.6}
\contentsline {chapter}{\numberline {7}Zaklju\IeC {\v c}ak}{33}{chapter.7}
\contentsline {chapter}{Literatura}{34}{chapter*.39}
