"""

"""
import abc
import os

import definitions
import fa.utils as utils
import fa.cvision as cvision
import fa.cvision.neural as neural
import fa.cvision.neural.engine as engine
import fa.cvision.neural.models as models
import fa.ui.gui as gui

TRAIN = 'train'
CMD = 'cmd'
ALPR = 'alpr'
GUI = 'gui'


def get(key):
    """

    :param key:
    :return:
    """
    return CommandProvider.get_instance()[key.lower()]


class CommandProvider(object):
    """

    """
    __UNLOCKED = False
    __instance = None

    def __init__(self):
        """

        """
        assert (CommandProvider.__UNLOCKED), "Cannot instantiate a " \
                                             "Singleton object"
        self.commands = {TrainCommand.NAME: TrainCommand(),
                         CMDCommand.NAME: CMDCommand(),
                         GUICommand.NAME: GUICommand(),
                         ALPRCommand.NAME: ALPRCommand()}

    @staticmethod
    def get_instance():
        """

        :return:
        """
        if not CommandProvider.__instance:
            CommandProvider.__UNLOCKED = True
            CommandProvider.__instance = CommandProvider()
            CommandProvider.__UNLOCKED = False

        return CommandProvider.__instance

    def get(self, key):
        """

        :param key:
        :return:
        """
        return self.commands[key.lower()]

    def __getattr__(self, item):
        """

        :param item:
        :return:
        """
        return self.get(item)

    def __getitem__(self, item):
        """

        :param item:
        :return:
        """
        return self.get(item)


class Command(abc.ABC):
    """

    """

    def __init__(self, name, desc=""):
        """

        :param name:
        :param desc:
        """
        self._name = name
        self._desc = desc

    @property
    def name(self):
        """

        :return:
        """
        return self._name

    @property
    def desc(self):
        """

        :return:
        """
        return self._desc

    @abc.abstractmethod
    def execute(self, args):
        """

        :param args:
        :return:
        """
        pass


class TrainCommand(Command):
    """

    """

    NAME = "train"
    DESC = ""

    def __init__(self):
        """

        """
        super().__init__(self.NAME, self.DESC)

    def execute(self, args):
        """

        :param args:
        :return:
        """
        return self._start(**args)

    def _start(self, dataset_dir, batch_size=128, epochs=5, save_dir=None,
               save_name=None, model_path=None):
        """

        :param dataset_dir:
        :param batch_size:
        :param epochs:
        :return:
        """
        try:
            (x_train, y_train), (x_test, y_test) = utils.load_from_files(
                dataset_dir)
        except:
            (x_train, y_train) = utils.load(os.path.join(dataset_dir,
                                                         'train_data'),
                                            dataset_dir, 'train')
            (x_test, y_test) = utils.load(os.path.join(dataset_dir,
                                                       'test_data'),
                                          dataset_dir, 'test')

        print('Data loaded.')

        num_classes = utils.num_of_classes(y_train)

        (x_train, y_train), (x_test, y_test), input_shape = \
            engine.prepare_training_data(x_train, y_train, x_test, y_test,
                                         num_classes)

        if not model_path:
            model = models.mnist_cnn(num_classes, input_shape)
        else:
            model = neural.load_model(model_path)
            print("JEEEEEEEJ")

        epochs = epochs if epochs else 5
        batch_size = batch_size if batch_size else 128

        model, score = engine.basic_training(model, x_train, y_train, x_test,
                                             y_test, batch_size, epochs)

        print('Test loss:', score[0])
        print('Test accuracy:', score[1])

        if save_dir:
            _save_model(save_dir, save_name, model)

        return model, score


class CMDCommand(Command):
    """

    """

    NAME = "cmd"
    DESC = ""

    def __init__(self):
        """

        """
        super().__init__(self.NAME, self.DESC)

    def execute(self, args):
        """

        :param args:
        :return:
        """
        return self._start(**args)

    def _start(self, sample_path, model_path=definitions.FIRST_MODEL_PATH):
        """

        :param model_path:
        :param sample_path:
        :return:
        """
        model = neural.load_model(model_path)

        if not os.path.exists(sample_path):
            raise ValueError('The specified path does not exist.')

        if os.path.isdir(sample_path):
            value = []
            for image_name in os.listdir(sample_path):
                value.append(
                    engine.predict_image(model, os.path.join(sample_path,
                                                             image_name)))

            value = [str(val + ', ') for val in value][:-2]

        else:
            value = engine.predict_image(model=model, image_path=sample_path)

        print(value)
        return value


class ALPRCommand(Command):
    """

    """

    NAME = "alpr"
    DESC = ""

    def __init__(self):
        """

        """
        super().__init__(self.NAME, self.DESC)

    def execute(self, args):
        """

        :param args:
        :return:
        """
        sample_path = args['sample_path']
        country = args.get('country', None)
        cvision.alpr(sample_path, country)


class GUICommand(Command):
    """

    """

    NAME = "gui"
    DESC = ""

    def __init__(self):
        """

        """
        super().__init__(self.NAME, self.DESC)

    def execute(self, args):
        """

        :param args:
        :return:
        """
        gui.run()


def _save_model(save_dir, save_name, model):
    """

    :param save_dir:
    :param save_name:
    :return:
    """
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    if not save_name:
        save_name = 'model.h5'
    elif not save_name.endswith('.h5'):
        save_name += '.h5'

    model.save(os.path.join(save_dir, save_name))
