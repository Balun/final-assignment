# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'second_interface.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

import sys
import fa.ui.actions as actions

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_main_window(QtGui.QMainWindow):
    def setupUi(self, main_window):
        main_window.setObjectName(_fromUtf8("main_window"))
        main_window.resize(835, 610)
        self.centralwidget = QtGui.QWidget(main_window)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.tabs = QtGui.QTabWidget(self.centralwidget)
        self.tabs.setObjectName(_fromUtf8("tabs"))

        self._create_file_tab()
        self._create_picamera_tab()
        self.create_webcam_tab()
        self.create_train_tab()

        self.gridLayout.addWidget(self.tabs, 0, 0, 1, 1)
        main_window.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(main_window)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        main_window.setStatusBar(self.statusbar)

        self.retranslateUi(main_window)
        self.tabs.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def _create_file_tab(self):
        self.file_tab = QtGui.QWidget()
        self.file_tab.setObjectName(_fromUtf8("file_tab"))
        self.formLayout = QtGui.QFormLayout(self.file_tab)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.select_file_button = QtGui.QPushButton(self.file_tab)
        self.select_file_button.setObjectName(_fromUtf8("select_file_button"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole,
                                  self.select_file_button)
        self.selected_file_label = QtGui.QLabel(self.file_tab)
        self.selected_file_label.setObjectName(
            _fromUtf8("selected_file_label"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole,
                                  self.selected_file_label)
        self.selected_file_text = QtGui.QLineEdit(self.file_tab)
        self.selected_file_text.setReadOnly(False)
        self.selected_file_text.setObjectName(_fromUtf8("selected_file_text"))

        selected_file_action = actions.SelectFileAction(self,
                                                        self.selected_file_text)
        self.select_file_button.clicked.connect(selected_file_action.execute)

        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole,
                                  self.selected_file_text)
        self.select_model_button = QtGui.QPushButton(self.file_tab)
        self.select_model_button.setObjectName(
            _fromUtf8("select_model_button"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole,
                                  self.select_model_button)
        self.select_model_label = QtGui.QLabel(self.file_tab)
        self.select_model_label.setObjectName(_fromUtf8("select_model_label"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole,
                                  self.select_model_label)
        self.select_model_text = QtGui.QLineEdit(self.file_tab)
        self.select_model_text.setObjectName(_fromUtf8("select_model_text"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole,
                                  self.select_model_text)

        selected_model_action = actions.SelectFileAction(self,
                                                         self.select_model_text)
        self.select_model_button.clicked.connect(selected_model_action.execute)

        self.recognize_image_button = QtGui.QPushButton(self.file_tab)
        self.recognize_image_button.setObjectName(
            _fromUtf8("recognize_image_button"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole,
                                  self.recognize_image_button)
        self.output_label = QtGui.QLabel(self.file_tab)
        self.output_label.setObjectName(_fromUtf8("output_label"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole,
                                  self.output_label)
        self.output_text = QtGui.QLineEdit(self.file_tab)
        self.output_text.setReadOnly(True)
        self.output_text.setObjectName(_fromUtf8("output_text"))
        self.formLayout.setWidget(5, QtGui.QFormLayout.FieldRole,
                                  self.output_text)

        recognize_image_action = actions.ValidateImageAction(self,
                                                             self.output_text,
                                                             self.select_model_text,
                                                             self.selected_file_text)
        self.recognize_image_button.clicked.connect(
            recognize_image_action.execute)

        self.tabs.addTab(self.file_tab, _fromUtf8(""))

    def _create_picamera_tab(self):
        self.raspb_tab = QtGui.QWidget()
        self.raspb_tab.setObjectName(_fromUtf8("raspb_tab"))
        self.formLayout_6 = QtGui.QFormLayout(self.raspb_tab)
        self.formLayout_6.setObjectName(_fromUtf8("formLayout_6"))
        self.select_model_button_3 = QtGui.QPushButton(self.raspb_tab)
        self.select_model_button_3.setObjectName(
            _fromUtf8("select_model_button_3"))
        self.formLayout_6.setWidget(6, QtGui.QFormLayout.LabelRole,
                                    self.select_model_button_3)
        self.select_model_label_3 = QtGui.QLabel(self.raspb_tab)
        self.select_model_label_3.setObjectName(
            _fromUtf8("select_model_label_3"))
        self.formLayout_6.setWidget(6, QtGui.QFormLayout.FieldRole,
                                    self.select_model_label_3)
        self.select_model_text_3 = QtGui.QLineEdit(self.raspb_tab)
        self.select_model_text_3.setObjectName(
            _fromUtf8("select_model_text_3"))
        self.formLayout_6.setWidget(9, QtGui.QFormLayout.FieldRole,
                                    self.select_model_text_3)

        select_model_action = actions.SelectFileAction(self,
                                                       self.select_model_text_3)
        self.select_model_button_3.clicked.connect(select_model_action.execute)

        self.capture_output_label = QtGui.QLabel(self.raspb_tab)
        self.capture_output_label.setObjectName(
            _fromUtf8("capture_output_label"))
        self.formLayout_6.setWidget(12, QtGui.QFormLayout.FieldRole,
                                    self.capture_output_label)
        self.picamera_output_text = QtGui.QLineEdit(self.raspb_tab)
        self.picamera_output_text.setReadOnly(True)
        self.picamera_output_text.setObjectName(
            _fromUtf8("picamera_output_text"))
        self.formLayout_6.setWidget(13, QtGui.QFormLayout.FieldRole,
                                    self.picamera_output_text)
        self.open_picamera_button = QtGui.QPushButton(self.raspb_tab)
        self.open_picamera_button.setObjectName(
            _fromUtf8("open_picamera_button"))
        self.formLayout_6.setWidget(2, QtGui.QFormLayout.LabelRole,
                                    self.open_picamera_button)
        self.picamera_label = QtGui.QLabel(self.raspb_tab)
        self.picamera_label.setObjectName(_fromUtf8("picamera_label"))
        self.formLayout_6.setWidget(2, QtGui.QFormLayout.FieldRole,
                                    self.picamera_label)
        self.lineEdit = QtGui.QLineEdit(self.raspb_tab)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.formLayout_6.setWidget(3, QtGui.QFormLayout.FieldRole,
                                    self.lineEdit)
        self.picamera_capture_button = QtGui.QPushButton(self.raspb_tab)
        self.picamera_capture_button.setObjectName(
            _fromUtf8("picamera_capture_button"))
        self.formLayout_6.setWidget(12, QtGui.QFormLayout.LabelRole,
                                    self.picamera_capture_button)

        picamera_capture_action = actions.CapturePICameraAction(self,
                                                                self.picamera_output_text,
                                                                self.select_model_text_3)
        self.picamera_capture_button.clicked.connect(
            picamera_capture_action.execute)

        self.tabs.addTab(self.raspb_tab, _fromUtf8(""))

    def create_webcam_tab(self):
        self.web_tab = QtGui.QWidget()
        self.web_tab.setObjectName(_fromUtf8("web_tab"))
        self.formLayout_3 = QtGui.QFormLayout(self.web_tab)
        self.formLayout_3.setObjectName(_fromUtf8("formLayout_3"))
        self.open_camera_button = QtGui.QPushButton(self.web_tab)
        self.open_camera_button.setObjectName(_fromUtf8("open_camera_button"))
        self.formLayout_3.setWidget(0, QtGui.QFormLayout.LabelRole,
                                    self.open_camera_button)
        self.camera_label = QtGui.QLabel(self.web_tab)
        self.camera_label.setObjectName(_fromUtf8("camera_label"))

        self.formLayout_3.setWidget(0, QtGui.QFormLayout.FieldRole,
                                    self.camera_label)
        self.select_camera_model_button = QtGui.QPushButton(self.web_tab)
        self.select_camera_model_button.setObjectName(
            _fromUtf8("select_camera_model_button"))
        self.formLayout_3.setWidget(1, QtGui.QFormLayout.LabelRole,
                                    self.select_camera_model_button)
        self.select_camera_model_text = QtGui.QLineEdit(self.web_tab)
        self.select_camera_model_text.setObjectName(
            _fromUtf8("select_camera_model_text"))
        self.formLayout_3.setWidget(1, QtGui.QFormLayout.FieldRole,
                                    self.select_camera_model_text)
        self.camera_capture_button = QtGui.QPushButton(self.web_tab)
        self.camera_capture_button.setObjectName(
            _fromUtf8("camera_capture_button"))

        self.formLayout_3.setWidget(2, QtGui.QFormLayout.LabelRole,
                                    self.camera_capture_button)
        self.camera_outuput_text = QtGui.QLineEdit(self.web_tab)
        self.camera_outuput_text.setObjectName(
            _fromUtf8("camera_outuput_text"))
        self.formLayout_3.setWidget(2, QtGui.QFormLayout.FieldRole,
                                    self.camera_outuput_text)
        self.tabs.addTab(self.web_tab, _fromUtf8(""))

    def create_train_tab(self):
        self.train_tab = QtGui.QWidget()
        self.train_tab.setObjectName(_fromUtf8("train_tab"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.train_tab)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))

        self.model_label = QtGui.QLabel(self.train_tab)
        self.model_label.setObjectName(_fromUtf8("model_label"))
        self.horizontalLayout.addWidget(self.model_label)
        self.model_text = QtGui.QLineEdit(self.train_tab)
        self.model_text.setObjectName(_fromUtf8("model_text"))
        self.horizontalLayout.addWidget(self.model_text)
        self.model_button = QtGui.QPushButton(self.train_tab)
        self.model_button.setObjectName(_fromUtf8("model_button"))
        self.horizontalLayout.addWidget(self.model_button)

        select_model_action = actions.SelectFileAction(self, self.model_text)
        self.model_button.clicked.connect(select_model_action.execute)

        self.verticalLayout_2.addLayout(self.horizontalLayout)
        spacerItem = QtGui.QSpacerItem(792, 20, QtGui.QSizePolicy.Expanding,
                                       QtGui.QSizePolicy.Minimum)
        self.verticalLayout_2.addItem(spacerItem)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.dataset_label = QtGui.QLabel(self.train_tab)
        self.dataset_label.setObjectName(_fromUtf8("dataset_label"))
        self.horizontalLayout_7.addWidget(self.dataset_label)
        self.dataset_text = QtGui.QLineEdit(self.train_tab)
        self.dataset_text.setObjectName(_fromUtf8("dataset_text"))
        self.horizontalLayout_7.addWidget(self.dataset_text)
        self.dataset_button = QtGui.QPushButton(self.train_tab)
        self.dataset_button.setObjectName(_fromUtf8("dataset_button"))
        self.horizontalLayout_7.addWidget(self.dataset_button)

        select_dataset_action = actions.SelectDirAction(self,
                                                        self.dataset_text)
        self.dataset_button.clicked.connect(select_dataset_action.execute)

        self.verticalLayout_2.addLayout(self.horizontalLayout_7)
        spacerItem1 = QtGui.QSpacerItem(792, 20, QtGui.QSizePolicy.Expanding,
                                        QtGui.QSizePolicy.Minimum)
        self.verticalLayout_2.addItem(spacerItem1)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.batch_label = QtGui.QLabel(self.train_tab)
        self.batch_label.setObjectName(_fromUtf8("batch_label"))
        self.horizontalLayout_8.addWidget(self.batch_label)
        self.batch_text = QtGui.QLineEdit(self.train_tab)
        self.batch_text.setObjectName(_fromUtf8("batc_text"))
        self.horizontalLayout_8.addWidget(self.batch_text)

        self.epochs_label = QtGui.QLabel(self.train_tab)
        self.epochs_label.setObjectName(_fromUtf8("epochs_label"))
        self.horizontalLayout_8.addWidget(self.epochs_label)
        self.epochs_text = QtGui.QLineEdit(self.train_tab)
        self.epochs_text.setObjectName(_fromUtf8("epochs_text"))
        self.horizontalLayout_8.addWidget(self.epochs_text)
        self.verticalLayout_2.addLayout(self.horizontalLayout_8)
        spacerItem2 = QtGui.QSpacerItem(792, 20, QtGui.QSizePolicy.Expanding,
                                        QtGui.QSizePolicy.Minimum)
        self.verticalLayout_2.addItem(spacerItem2)
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))

        self.save_dir_label = QtGui.QLabel(self.train_tab)
        self.save_dir_label.setObjectName(_fromUtf8("save_dir_label"))
        self.horizontalLayout_9.addWidget(self.save_dir_label)
        self.save_dir_text = QtGui.QLineEdit(self.train_tab)
        self.save_dir_text.setObjectName(_fromUtf8("save_dir_text"))
        self.horizontalLayout_9.addWidget(self.save_dir_text)
        self.save_dir_button = QtGui.QPushButton(self.train_tab)
        self.save_dir_button.setObjectName(_fromUtf8("save_dir_button"))
        self.horizontalLayout_9.addWidget(self.save_dir_button)
        self.verticalLayout_2.addLayout(self.horizontalLayout_9)

        select_save_dir_action = actions.SelectDirAction(self,
                                                         self.save_dir_text)
        self.save_dir_button.clicked.connect(select_save_dir_action.execute)

        spacerItem3 = QtGui.QSpacerItem(792, 20, QtGui.QSizePolicy.Expanding,
                                        QtGui.QSizePolicy.Minimum)
        self.verticalLayout_2.addItem(spacerItem3)
        self.horizontalLayout_10 = QtGui.QHBoxLayout()
        self.horizontalLayout_10.setObjectName(
            _fromUtf8("horizontalLayout_10"))
        self.save_name_label = QtGui.QLabel(self.train_tab)
        self.save_name_label.setObjectName(_fromUtf8("save_name_label"))
        self.horizontalLayout_10.addWidget(self.save_name_label)
        self.save_name_text = QtGui.QLineEdit(self.train_tab)
        self.save_name_text.setObjectName(_fromUtf8("save_name_text"))
        self.horizontalLayout_10.addWidget(self.save_name_text)
        self.verticalLayout_2.addLayout(self.horizontalLayout_10)
        spacerItem4 = QtGui.QSpacerItem(792, 20, QtGui.QSizePolicy.Expanding,
                                        QtGui.QSizePolicy.Minimum)
        self.verticalLayout_2.addItem(spacerItem4)
        self.horizontalLayout_11 = QtGui.QHBoxLayout()
        self.horizontalLayout_11.setObjectName(
            _fromUtf8("horizontalLayout_11"))
        self.train_button_2 = QtGui.QPushButton(self.train_tab)
        self.train_button_2.setObjectName(_fromUtf8("train_button_2"))
        self.horizontalLayout_11.addWidget(self.train_button_2)
        self.verticalLayout_2.addLayout(self.horizontalLayout_11)

        train_action = actions.TrainAction(self)
        self.train_button_2.clicked.connect(train_action.execute)

        self.tabs.addTab(self.train_tab, _fromUtf8(""))

    def retranslateUi(self, main_window):
        main_window.setWindowTitle(
            _translate("main_window", "Final Assignment", None))
        self.select_file_button.setText(
            _translate("main_window", "Select file", None))
        self.selected_file_label.setText(
            _translate("main_window", "Selected file:", None))
        self.select_model_button.setText(
            _translate("main_window", "Select model", None))
        self.select_model_label.setText(
            _translate("main_window", "Selected model:", None))
        self.recognize_image_button.setText(
            _translate("main_window", "Recognize image", None))
        self.output_label.setText(_translate("main_window", "output:", None))
        self.tabs.setTabText(self.tabs.indexOf(self.file_tab),
                             _translate("main_window", "Recognize file", None))
        self.select_model_button_3.setText(
            _translate("main_window", "Select model", None))
        self.select_model_label_3.setText(
            _translate("main_window", "Selected model:", None))
        self.capture_output_label.setText(
            _translate("main_window", "output:", None))
        self.open_picamera_button.setText(
            _translate("main_window", "Open PIcamera", None))
        self.picamera_label.setText(
            _translate("main_window", "PIcamera closed", None))
        self.picamera_capture_button.setText(
            _translate("main_window", "Capture and recognize", None))
        self.tabs.setTabText(self.tabs.indexOf(self.raspb_tab),
                             _translate("main_window", "Raspberry PI", None))
        self.open_camera_button.setText(
            _translate("main_window", "Open camera", None))
        self.camera_label.setText(
            _translate("main_window", "Camera closed", None))
        self.select_camera_model_button.setText(
            _translate("main_window", "select model:", None))
        self.camera_capture_button.setText(
            _translate("main_window", "Capture and recognize", None))
        self.tabs.setTabText(self.tabs.indexOf(self.web_tab),
                             _translate("main_window", "WebCam", None))
        self.model_label.setText(
            _translate("main_window", "Select model:", None))
        self.model_button.setText(_translate("main_window", "browse...", None))
        self.dataset_label.setText(
            _translate("main_window", "Dataset directory:", None))
        self.dataset_button.setText(
            _translate("main_window", "browse...", None))
        self.batch_label.setText(
            _translate("main_window", "Batch size:", None))
        self.epochs_label.setText(_translate("main_window", "Epochs:", None))
        self.save_dir_label.setText(
            _translate("main_window", "Save directory:", None))
        self.save_dir_button.setText(
            _translate("main_window", "browse...", None))
        self.save_name_label.setText(
            _translate("main_window", "Save name:", None))
        self.train_button_2.setText(_translate("main_window", "Train", None))
        self.tabs.setTabText(self.tabs.indexOf(self.train_tab),
                             _translate("main_window", "Train", None))


def run():
    """

    :return:
    """
    app = QtGui.QApplication(sys.argv)
    main_window = QtGui.QMainWindow()
    ui = Ui_main_window()
    ui.setupUi(main_window)
    main_window.show()
    sys.exit(app.exec_())
