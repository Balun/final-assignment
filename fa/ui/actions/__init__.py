"""

"""
import os
import threading

import PyQt4.QtGui as gui

import fa.ui as ui
import fa.inputs.webcam as webcam
import fa.inputs.pi.raspb_camera as picamera
import definitions
import fa.utils as utils

from fa.cvision import show_image, read_image


class AbstractAction(gui.QAction):
    """

    """

    def __init__(self, name, parent):
        """

        :param name:
        :param parent:
        """
        super().__init__(name, parent)
        self.triggered.connect(self.execute)

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        return NotImplementedError()


class SelectFileAction(AbstractAction):
    """

    """

    def __init__(self, parent, text_field):
        """

        :param parent:
        """
        super().__init__('Select file', parent)
        self.text_field = text_field

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        name = gui.QFileDialog.getOpenFileName(self.parent(), 'Open file',
                                               directory=definitions.ROOT_DIR)

        if not (os.path.exists(name) or os.path.isfile(name)):
            ui.error_message("The specified file does not exist or it's a "
                             "directory", self.parent())
            return

        self.text_field.setText(name)
        return name


class SelectDirAction(AbstractAction):
    """

    """

    def __init__(self, parent, text_field):
        """

        :param parent:
        """
        super().__init__('Select directory', parent)
        self.text_field = text_field

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        name = gui.QFileDialog.getExistingDirectory(self.parent(),
                                                    'Open directory',
                                                    directory=definitions.ROOT_DIR)

        if not (os.path.exists(name) or os.path.isfile(name)):
            ui.error_message("The specified path does not exist or it's "
                             "not a directory", self.parent())
            return

        self.text_field.setText(name)
        return name


class TrainAction(AbstractAction):
    """

    """

    def __init__(self, parent):
        """

        :param parent:
        """
        super().__init__('Train', parent)

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        dataset_dir = self.parent().dataset_text.text()

        batch_size = self.parent().batch_text.text()
        batch_size = int(batch_size) if batch_size else None

        epochs = self.parent().epochs_text.text()
        epochs = int(epochs) if epochs else None

        save_dir = self.parent().save_dir_text.text()
        save_dir = save_dir if save_dir else None

        save_name = self.parent().save_name_text.text()
        save_name = save_name if save_name else None

        model_path = self.parent().model_text.text()
        model_path = model_path if model_path else None

        args = {'dataset_dir': dataset_dir,
                'batch_size': batch_size,
                'epochs': epochs,
                'save_dir': save_dir,
                'save_name': save_name,
                'model_path': model_path}

        def run(action, args):
            import fa.commands as commands
            action.output = commands.get(commands.TRAIN).execute(args)

        thread = threading.Thread(target=run, args=(self, args,))
        thread.start()
        thread.join()

        ui.message('Test loss: {}\nTest accuracy: {}'.format(self.output[0],
                                                             self.output[1]))
        return self.output


class ValidateImageAction(AbstractAction):
    """

    """

    def __init__(self, parent, text_field, model_path, file_path):
        """

        :param parent:
        """
        super().__init__("Validate image or directory of images", parent)
        self.text_field = text_field
        self.model_path_field = model_path
        self.file_path_field = file_path

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        arg = {'sample_path': self.file_path_field.text(),
               'model_path': self.model_path_field.text()}

        """
        def run(action, args):
            import fa.commands as commands
            action.output = commands.get(commands.CMD).execute(args)

        thread = threading.Thread(target=run, args=(self, arg,))
        thread.start()
        thread.join()
        """
        import fa.commands as commands
        try:
            self.output = commands.get(commands.CMD).execute(arg)
        except utils.PlateRecognitionError as ex:
            ui.error_message(str(ex), self.parent())
            return ""

        self.text_field.setText(self.output)

        show_image(read_image(arg['sample_path']), arg['sample_path'])
        return self.output


class OpenWebCamAction(AbstractAction):
    """

    """

    def __init__(self, parent, label):
        """

        """
        super().__init__('Open WebCam', parent)
        self.label = label

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        camera = webcam.WebCam.get_instance()
        self.label.setText('Camera opened')
        img_file = camera.open_preview()

        if img_file:
            self.text_field.setText(img_file)

        self.label.setText('camera closed')


class CloseWebCamAction(AbstractAction):
    """
    
    """

    def __init__(self, parent, label):
        """

        :param parent:
        :param label:
        """
        super().__init__('Close webcam', parent)
        self.label = label

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        webcam.WebCam.get_instance().close_preview()


class OpenPICameraAction(AbstractAction):
    """

    """

    def __init__(self, parent, label):
        """

        :param parent:
        :param label:
        """
        super().__init__('Open picamera', parent)
        self.label = label

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        picamera.RaspbCamera.get_instance().open_preview()


class ClosePIcameraAction(AbstractAction):
    """

    """

    def __init__(self, parent, label):
        """

        :param parent:
        :param label:
        """
        super().__init__('CLose picamera', parent)
        self.label = label

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        picamera.RaspbCamera.get_instance().close_preview()


class CapturePICameraAction(AbstractAction):
    """

    """

    def __init__(self, parent, text_field, model_text_field):
        """

        """
        super().__init__('Capture and validate image', parent)
        self.text_field = text_field
        self.model_text_field = model_text_field

    def execute(self, **args):
        """

        :param args:
        :return:
        """
        file_path = picamera.RaspbCamera.get_instance().capture()

        validate_action = ValidateImageAction(self.parent(),
                                              self.text_field,
                                              self.model_text_field.text(),
                                              file_path)

        validate_action.execute()
