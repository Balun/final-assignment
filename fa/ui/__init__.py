import PyQt4.QtGui as qgui


def error_message(text, parent=None):
    """

    :param text:
    :param parent:
    :return:
    """
    msgBox = qgui.QMessageBox(parent)
    msgBox.setWindowTitle("Error")
    msgBox.setIcon(qgui.QMessageBox.Critical)
    msgBox.setText(text)

    msgBox.addButton(qgui.QMessageBox.Cancel)
    msgBox.setDefaultButton(qgui.QMessageBox.Cancel)

    msgBox.exec_()


def message(text, parent=None):
    """

    :param text:
    :param parent:
    :return:
    """
    msgBox = qgui.QMessageBox(parent)
    msgBox.setWindowTitle("Information")
    msgBox.setIcon(qgui.QMessageBox.Information)
    msgBox.setText(text)

    msgBox.addButton(qgui.QMessageBox.Cancel)
    msgBox.setDefaultButton(qgui.QMessageBox.Cancel)

    msgBox.exec_()
