"""

"""

import sys
import os

import openalpr
import cv2 as cv

import definitions
import fa.utils as utils

CONTOUR_UPPER_BOUND = 0.1
CONTOUR_LOWER_BOUND = 0.025

DEFAULT_COUNTRY = 'eu'


def crop_licence_plate(image_path, country=DEFAULT_COUNTRY):
    """

    :param image_path:
    :param country:
    :return:
    """
    alpr = openalpr.Alpr(country, definitions.OPENALPR_CONF_PATH,
                         definitions.RUNTIME_DATA_PATH)

    if not alpr.is_loaded():
        print("Error loading OpenALPR")
        sys.exit(1)

    alpr.set_top_n(1)
    alpr.set_default_region("md")

    image = cv.imread(image_path)
    x, y, width, height = _get_crop_values(_get_crop_coordinates(alpr, image))

    # Crop the licence plate
    plate = image[y:y + height, x:x + width]

    alpr.unload()
    return plate


def crop_licence_elements(plate_img):
    """

    :param plate_img:
    :param country:
    :return:
    """

    plate_img = grayscale_to_binary(convert_to_grayscale(plate_img))
    height, width = plate_img.shape

    im2, contours, hierarchy = cv.findContours(plate_img, cv.RETR_LIST,
                                               cv.CHAIN_APPROX_NONE)
    boxes = _sort_contours(contours)[1]

    # box = (x, y, w, h)
    return [plate_img[box[1]:box[1] + box[3], box[0]:box[0] + box[2]] for
            box in boxes if _check_character_element(box[2] * box[3],
                                                     width * height)]


def convert_to_grayscale(image):
    """

    :param image:
    :return:
    """
    return cv.cvtColor(image, cv.COLOR_BGR2GRAY)


def grayscale_to_binary(image):
    """

    :param image:
    :return:
    """
    return cv.threshold(image, 128, 255, cv.THRESH_BINARY |
                        cv.THRESH_OTSU)[1]


def invert_colors(image):
    """

    :param image:
    :return:
    """
    return cv.bitwise_not(image)


def resize(image, new_width, new_height):
    """

    :param image:
    :param width:
    :param height:
    :return:
    """
    height, width = image.shape[:2]
    return cv.resize(image, (new_width, new_height))


def read_image(file_path, grayscale=True):
    """

    :param file_path:
    :param grayscale:
    :return:
    """
    return cv.imread(file_path, 0 if grayscale else 1)


def show_image(image, window_name='example'):
    """

    :param file_path:
    :return:
    """
    cv.imshow(window_name, image)
    cv.waitKey(0)
    cv.destroyAllWindows()


def alpr(image_path, country=DEFAULT_COUNTRY):
    """

    :param image_path:
    :param country:
    :return:
    """
    alpr = openalpr.Alpr(country if country else DEFAULT_COUNTRY,
                         definitions.OPENALPR_CONF_PATH,
                         definitions.RUNTIME_DATA_PATH)

    if not alpr.is_loaded():
        print("Error loading OpenALPR")
        sys.exit(1)

    alpr.set_top_n(1)
    alpr.set_default_region("md")

    if os.path.isdir(image_path):
        for im_path in os.listdir(image_path):
            results = alpr.recognize_file(os.path.join(image_path, im_path))
            print(results['results'][0]['plate'])

    else:
        results = alpr.recognize_file(image_path)
        print(results['results'][0]['plate'])

    alpr.unload()
    return results


def _get_crop_values(coordinates):
    """

    :param coordinates:
    :return:
    """
    x = coordinates[0]['x']
    y = coordinates[0]['y']
    width = coordinates[1]['x'] - x
    height = coordinates[2]['y'] - y

    return x, y, width, height


def _get_crop_coordinates(alpr, image):
    """

    :param image:
    :return:
    """
    try:
        return alpr.recognize_ndarray(image)[
            'results'][0]['coordinates']
    except IndexError:
        raise utils.PlateRecognitionError('Unable to recognize plate')


def _sort_contours(contours):
    bounding_boxes = [cv.boundingRect(c) for c in contours]
    (contours, bounding_boxes) = zip(*sorted(zip(contours, bounding_boxes),
                                             key=lambda b: b[1][0]))

    return contours, bounding_boxes


def _check_character_element(contour_area, plate_area):
    """

    :param countur_area:
    :param plate_area:
    :return:
    """
    return CONTOUR_LOWER_BOUND <= contour_area / plate_area <= \
           CONTOUR_UPPER_BOUND
