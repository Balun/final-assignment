"""

"""

import keras.backend as backend
import numpy as np

import fa.utils as utils
import fa.cvision as cvision

img_rows, img_cols = 32, 24


def basic_training(model, x_train, y_train, x_test, y_test, batch_size=128,
                   epoch=5):
    """
    Train the specified model with its provided learning parameters.

    :param model: The specified deep learning model object
    :param x_train: the sample data from training set
    :param y_train: the labeled data from training set
    :param x_test: the sample data from test set
    :param y_test: the labeled data from test set
    :param batch_size: size of the training batch
    :param epoch: number of epochs for training
    :return: tuple containing the model object and result of evaluation
    """
    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epoch,
              verbose=1,
              validation_data=(x_test, y_test))
    return model, model.evaluate(x_test, y_test, verbose=0)


def prepare_training_data(x_train, y_train, x_test, y_test,
                          num_classes):
    """

    :param x_train:
    :param y_train:
    :param x_test:
    :param y_test:
    :param input_shape:
    :return:
    """
    x_train = np.array([cvision.grayscale_to_binary(img) for img in x_train])
    x_train = cvision.invert_colors(x_train)
    x_test = np.array([cvision.grayscale_to_binary(img) for img in x_test])
    x_test = cvision.invert_colors(x_test)

    if backend.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)

    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255

    # convert class vectors to binary class matrices
    y_train = utils.to_categorical(y_train, num_classes)
    y_test = utils.to_categorical(y_test, num_classes)

    return (x_train, y_train), (x_test, y_test), input_shape


def prepare_prediction_sample(x):
    """

    :param x:
    :return:
    """
    x = cvision.invert_colors(cvision.resize(x, img_cols, img_rows))
    if backend.image_data_format() == 'channels_first':
        x = x.reshape(1, img_rows, img_cols)

    else:
        x = x.reshape(img_rows, img_cols, 1)

    x = x.astype('float32')
    x /= 255

    return np.array([x])


def predict(model, sample):
    """

    :param model:
    :param sample:
    :return:
    """
    sample = prepare_prediction_sample(sample)
    return utils.get_char(model.predict(sample)[0])


def predict_plate(model, plate):
    """

    :param late:
    :return:
    """
    elements = cvision.crop_licence_elements(plate)
    result = ''

    for elem in elements:
        result += predict(model, elem)

    return result


def predict_image(model, image_path):
    """

    :param model:
    :param plate:
    :return:
    """
    plate = cvision.crop_licence_plate(image_path)
    return predict_plate(model, plate)
