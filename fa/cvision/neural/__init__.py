"""

"""

import keras


def load_model(model_file_path):
    """

    :param model_file:
    :return:
    """
    return keras.models.load_model(model_file_path)
