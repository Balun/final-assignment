"""

"""

import keras
import keras.layers as layers
import keras.models as models


def mnist_cnn(num_of_classes, input_shape):
    """
    Creates the simple deep learning model based on the provided number of
    classes and input shape of the data.

    :param num_of_classes: number of labels for the model
    :param input_shape: the sample data input shape format
    :return: new instance of convolutional model
    """
    model = models.Sequential()

    model.add(layers.Conv2D(32, kernel_size=(3, 3),
                            activation='relu',
                            input_shape=input_shape))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(layers.Dropout(0.25))
    model.add(layers.Flatten())
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(num_of_classes, activation='softmax'))

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    return model
