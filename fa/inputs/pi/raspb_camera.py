#import picamera
import time
import shutil
import os

import fa.inputs.camera as camera
import definitions


class RaspbCamera(camera.Camera):
    NAME = 'picamera'
    SLEEP_INTERVAL = 2
    PATH = os.path.join(definitions.ROOT_DIR, 'tmp',
                        'picamera')

    __instance = None
    __locked = True

    def __init__(self, dir_path=None):
        if RaspbCamera.__locked:
            raise PermissionError("Cannot instantiate a singleton class!")
        super().__init__(self.NAME, dir_path)
        self._camera = picamera.PiCamera()

    @staticmethod
    def get_instance():
        if not RaspbCamera.__instance:
            RaspbCamera.__locked = False
            RaspbCamera.__instance = RaspbCamera(RaspbCamera.PATH)
            RaspbCamera.__locked = True

    def open_preview(self):
        """

        :return:
        """
        self._camera.start_preview()

    def close_preview(self):
        """

        :return:
        """
        self._camera.stop_preview()

    def capture(self, file_path=None, text=None):
        file_path = self.get_path_img(file_path)

        time.sleep(self.SLEEP_INTERVAL)
        self._camera.annotate_text = text
        self._camera.capture(file_path)

        return file_path

    def record(self, record_time, file_path=None, text=None):
        file_path = self.get_path_vid(file_path)

        self._camera.annotate_text = text
        self._camera.start_recording(file_path)
        time.sleep(record_time)
        self._camera.stop_recording()

        return file_path

    def start_recording(self, file_path=None, text=None):
        file_path = self.get_path_img(file_path)

        self._camera.annotate_text = text
        self._camera.start_recording(file_path)

        return file_path

    def stop_recording(self):
        self._camera.stop_recording()

    @property
    def resolution(self):
        return self._camera.resolution

    @resolution.setter
    def resolution(self, res):
        self._camera.resolution = res

    @property
    def frame_rate(self):
        return self._camera.framerate

    @frame_rate.setter
    def frame_rate(self, rate):
        self._camera.framerate = rate

    @property
    def brightness(self):
        return self._camera.brightness

    @brightness.setter
    def brightness(self, bright):
        self._camera.brightness = bright

    @property
    def text_size(self):
        return self._camera.annotate_text_size

    @text_size.setter
    def text_size(self, size):
        self._camera.annotate_text_size = size

    @property
    def image_effect(self):
        return self._camera.image_effect

    @image_effect.setter
    def image_effect(self, effect):
        self._camera.image_effect = effect

    def __del__(self):
        shutil.rmtree(RaspbCamera.PATH)
