import abc
import os


class Camera(abc.ABC):
    PATH_IMG = lambda dir, name, i: os.path.join(dir, name + str(i) + '.png')
    PATH_VID = lambda dir, name, i: os.path.join(dir, name + str(i) + '.h264')

    def __init__(self, name, dir_path=None):
        self.index = 0
        self._name = name

        self._dir = dir_path if dir_path else os.getcwd()
        if not os.path.exists(self._dir):
            os.makedirs(self.dir)

    @abc.abstractmethod
    def open_preview(self):
        raise NotImplementedError

    @abc.abstractmethod
    def close_preview(self):
        raise NotImplementedError

    @abc.abstractmethod
    def capture(self, file_path=None, text=None):
        raise NotImplementedError

    @abc.abstractmethod
    def record(self, time, file_path=None, text=None):
        raise NotImplementedError

    @abc.abstractmethod
    def start_recording(self, file_path=None, text=None):
        raise NotImplementedError

    def stop_recording(self):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def resolution(self):
        raise NotImplementedError

    @resolution.setter
    @abc.abstractmethod
    def resolution(self, res):
        raise NotImplementedError

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        self._name = new_name

    @property
    def dir(self):
        return self._dir

    @dir.setter
    def dir(self, dir_path):
        if not os.path.exists(dir_path):
            raise ValueError('The specified path does not exist.')

        elif not os.path.isdir(dir_path):
            raise NotADirectoryError

        self._dir = dir_path

    @property
    @abc.abstractmethod
    def frame_rate(self):
        raise NotImplementedError

    @frame_rate.setter
    @abc.abstractmethod
    def frame_rate(self, rate):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def brightness(self):
        raise NotImplementedError

    @brightness.setter
    @abc.abstractmethod
    def brightness(self, bright):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def text_size(self):
        raise NotImplementedError

    @text_size.setter
    @abc.abstractmethod
    def text_size(self, size):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def image_effect(self):
        raise NotImplementedError

    @image_effect.setter
    @abc.abstractmethod
    def image_effect(self, effect):
        raise NotImplementedError

    def get_path_vid(self, file_path=None):
        file_path = file_path if file_path else os.path.join(self.dir,
                                                             self.name + str(
                                                                 self.index)
                                                             + '.h264')
        self.index += 1
        return file_path

    def get_path_img(self, file_path=None):
        file_path = file_path if file_path else os.path.join(self.dir,
                                                             self.name + str(
                                                                 self.index)
                                                             + '.png')

        self.index += 1
        return file_path
