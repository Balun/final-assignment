import cv2 as cv
import time
import shutil
import os

import fa.inputs.camera as camera
import fa.cvision as cvision
import definitions


class WebCam(camera.Camera):
    NAME = 'webcam'
    PATH = os.path.join(definitions.ROOT_DIR, 'tmp', 'webcam')

    __instance = None
    __locked = True

    def __init__(self, dir_path, cam_index=1):
        if WebCam.__locked:
            raise PermissionError("Cannot instantiate a singleton class")
        super().__init__(self.NAME, dir_path)

        try:
            self._camera = cv.VideoCapture(cam_index)
        except:
            self._camera = cv.VideoCapture(0)

        self.opened = False

    @staticmethod
    def get_instance():
        if not WebCam.__instance:
            WebCam.__locked = False
            WebCam.__instance = WebCam(WebCam.PATH)
            WebCam.locked = True

        return WebCam.__instance

    def open_preview(self):
        """

        :return:
        """

        self.opened = True
        while True:
            s, img = self._camera.read()
            try:
                cv.imshow('preview', img)
            except cv.error:
                continue
            if cv.waitKey(1) == 13:
                p = self.get_path_img()
                cv.imwrite(p, img)
                self.close_preview()
                return p

            elif cv.waitKey(1) == 27:
                self.close_preview()
                return None
            elif not self.opened:
                p = self.get_path_img()
                cv.imwrite(p, img)
                self.close_preview()
                return p

    def close_preview(self):
        """

        :return:
        """
        self.opened = False
        self._camera.release()
        cv.destroyAllWindows()

    def capture(self, file_path=None, text=None):
        self._check_camera()

        file_path = self.get_path_img(file_path)

        s, image = self._camera.read()
        cv.imwrite(file_path, image)

        cvision.show_image(image, file_path)

        return file_path

    def record(self, record_time, file_path=None, text=None):
        self._check_camera()

        file_path = self.get_path_vid(file_path)
        out = cv.VideoWriter(file_path, -1, 20.0, (640, 480))

        while (self._camera.isOpened() and record_time > 0):
            time.sleep(1)

            ret, frame = self._camera.read()
            if ret:
                frame = cv.flip(frame, 0)

                out.write(frame)

                if cv.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break

        self._camera.release()
        out.release()

    def start_recording(self, file_path=None, text=None):
        raise NotImplementedError

    def stop_recording(self):
        raise NotImplementedError

    @property
    def resolution(self):
        return self._camera.get(cv.CAP_PROP_FRAME_WIDTH), self._camera.get(
            cv.CAP_PROP_FRAME_HEIGHT)

    @resolution.setter
    def resolution(self, res):
        self._camera.set(cv.CAP_PROP_FRAME_WIDTH, res[0])
        self._camera.set(cv.CAP_PROP_FRAME_HEIGHT, res[1])

    @property
    def frame_rate(self):
        return self._camera.get(cv.CAP_PROP_FPS)

    @frame_rate.setter
    def frame_rate(self, rate):
        self._camera.set(cv.CAP_PROP_FPS, rate)

    @property
    def brightness(self):
        return self._camera.get(cv.CAP_PROP_BRIGHTNESS)

    @brightness.setter
    def brightness(self, value):
        self._camera.set(cv.CAP_PROP_BRIGHTNESS, value)

    @property
    def text_size(self):
        raise NotImplementedError

    @property
    def image_effect(self):
        raise NotImplementedError

    def __del__(self):
        shutil.rmtree(WebCam.PATH)
        self._camera.release()

    def _check_camera(self):
        if not self._camera.isOpened():
            raise IOError('The camera stream is not opened')
