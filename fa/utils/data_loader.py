"""

"""

import os
import pathlib

import numpy as np
import cv2 as cv


def load(dir_name, save_dir=None, label='train', grayscale=True):
    """

    :param dir_name:
    :param save_dir:
    :param grayscale:
    :return:
    """
    if not (os.path.exists(dir_name) or os.path.isdir(dir_name)):
        raise IOError('The specified path does not exist or it is not a '
                      'directory')

    x_data = []
    y_data = []

    for class_dir in sorted(os.listdir(dir_name)):
        cv.waitKey(0)

        for elem in os.listdir(os.path.join(dir_name, class_dir)):
            img = cv.imread(os.path.join(dir_name, class_dir, elem),
                            0 if grayscale else 1)

            x_data.append(img)
            y_data.append(os.path.split(class_dir)[-1])

    x_data, y_data = np.array(x_data), np.array(y_data)

    if save_dir:
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        if os.path.isdir(save_dir):
            np.save(os.path.join(save_dir, 'x_' + label + '.npy'), x_data)
            np.save(os.path.join(save_dir, 'y_' + label + '.npy'), y_data)

    return x_data, y_data


def load_from_files(dir_path):
    """

    :param dir_name:
    :return:
    """
    dir_path = pathlib.Path(dir_path)
    x_train = np.load(dir_path / 'x_train.npy')
    y_train = np.load(dir_path / 'y_train.npy')
    x_test = np.load(dir_path / 'x_test.npy')
    y_test = np.load(dir_path / 'y_test.npy')

    return (x_train, y_train), (x_test, y_test)
