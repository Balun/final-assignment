"""

"""

from fa.utils.data_loader import *

import locale
import numpy as np
import keras.utils as np_utils

CHARS = '0123456789ABCDEFGHIJKLMNOPRSTUVZĆČĐŠŽ'


def to_numerical(array):
    """

    :param character:
    :return:
    """
    return np.array([CHARS.find(char) for char in array])


def to_categorical(array, num_of_classes):
    return np_utils.to_categorical(to_numerical(array), num_of_classes)


def get_char(array):
    """

    :param array:
    :return:
    """
    index = np.argmax(array)
    return CHARS[int(index)]


def num_of_classes(y_data):
    """

    :param y_data:
    :return:
    """
    return len(np.unique(y_data))


class PlateRecognitionError(ValueError):
    """

    """
    pass
