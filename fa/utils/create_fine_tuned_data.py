import os
import sys
import ast
import urllib.request as request

import fa.cvision as cvision
import cv2 as cv

ANNOTATION = 'annotation'
CONTENT = 'content'
LABELS = 'labels'


def main(data_dir, output_dir):
    """

    :return:
    """
    i = 152
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    for img in os.listdir(data_dir):
        try:
            plate = cvision.crop_licence_plate(os.path.join(data_dir, img))

            for elem in cvision.crop_licence_elements(plate):
                cv.imwrite(os.path.join(output_dir, str(i) + '.jpg'),
                           cvision.resize(elem, 24, 32))
                i += 1
        except IndexError:
            continue


def read_json(json_path, output_dir):
    """

    :param json_path:
    :param output_dir:
    :return:
    """
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    f = open(json_path, 'r')

    i = 0
    for line in f:
        data = ast.literal_eval(line)
        url = data[CONTENT]

        response = request.urlopen(url)
        img_bytes = response.read()

        labels = data[ANNOTATION][LABELS]

        for label in labels:

            if not os.path.exists(os.path.join(output_dir, label)):
                os.mkdir(os.path.join(output_dir, label))

            with open(os.path.join(output_dir, label, str(i) + '.jpg'),
                      'wb') as img:
                img.write(img_bytes)

            print(i)
            i += 1


if __name__ == '__main__':
    read_json(sys.argv[1], sys.argv[2])
