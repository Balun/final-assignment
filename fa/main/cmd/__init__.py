import os

import definitions
import fa.cvision.neural as neural
import fa.cvision.neural.engine as engine


def start(sample_path, model_path=definitions.FIRST_MODEL_PATH):
    """

    :param model_path:
    :param sample_path:
    :return:
    """
    model = neural.load_model(model_path)

    if not os.path.exists(sample_path):
        raise ValueError('The specified path does not exist.')

    if os.path.isdir(sample_path):
        for image_name in os.listdir(sample_path):
            print(engine.predict_image(model, os.path.join(sample_path,
                                                           image_name)))

    else:
        print(engine.predict_image(model=model, image_path=sample_path))
